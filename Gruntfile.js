/**
 *     Author : yogesh.bansal
 * Created on : 1/23/16  2:25 PM.
 *
 */
module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        includeSource: {
            options: {
                basePath: 'app',
                baseUrl: 'app/'
            },
            myTarget: {
                files: {
                    'index.html': 'index.tpl.html'
                }
            }
        },
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'assets/sass',
                    src: ['main.scss'],
                    dest: 'assets/css',
                    ext: '.css'
                }]
            }
        },
        jshint: {
            files: ['app/**/*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },
        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: 'vendor/bootstrap-sass/assets/',
                    src: ['fonts/**/*'],
                    dest: 'assets/build/'
                }]
            }
        },
        cssmin: {
            combine:{
                files :{
                    'assets/build/css/<%= pkg.name %>-<%= pkg.version %>.css':['assets/css/*.css']
                }
            }
        },
        watch: {
            watch_js_files: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint','includeSource']
            },
            // Watching For Sass Changes
            watch_sass_files: {
                files : ['assets/sass/**/*.scss'],
                tasks : ['sass','cssmin','copy']
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: 'src/<%= pkg.name %>.js',
                dest: 'build/<%= pkg.name %>.min.js'
            }
        }
    });
    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-include-source');
    grunt.loadNpmTasks('grunt-contrib-copy');
    // Default task(s).
    //grunt.registerTask('default', ['uglify']);
    grunt.registerTask('default', ['watch']);

};