/**
 * Created by samiksha on 6/5/16.
 */


var mainApplicationModule = angular.module('grocery', ['ui.router','ui.bootstrap']);
mainApplicationModule.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
});