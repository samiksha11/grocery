/**
 * Created by samiksha on 6/5/16.
 */
angular.module('grocery').config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        // HOME STATES AND NESTED VIEWS ========================================
        //.state('home', {
        //    abstract: true,
        //    url: '/home',
        //    views: {
        //        '@': {
        //            templateUrl: 'app/views/layouts/layout.html',
        //            controller: 'HomeController'
        //        },
        //        'headerView@home': {
        //            templateUrl: 'app/views/layouts/header.html',
        //            controller: 'HomeController'
        //        },
        //        'sidebarView@home': {
        //            templateUrl: 'app/views/layouts/sidebar.html',
        //            controller: 'HomeController'
        //        },
        //        'footerView@home': {
        //            templateUrl: 'app/views/layouts/footer.html',
        //            controller: 'HomeController'
        //        }
        //    }
        //})
        .state('home', {
            url: '/home',
            templateUrl: 'views/home.html'
        });
});

